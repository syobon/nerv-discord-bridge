use html2md::parse_html;
use megalodon::{entities::status::Status, generator, streaming::Message, SNS};
use serde::Deserialize;
use std::fs;
use ureq::json;

#[derive(Clone, Deserialize)]
struct Config {
    webhook: Vec<String>,
    tags: Vec<String>,
}

#[tokio::main]
async fn main() {
    let Ok(config_text) = fs::read_to_string("config.toml") else {
        eprintln!("* config.toml not found!");
        return;
    };
    let Ok(config): Result<Config, _> = toml::from_str(&config_text) else {
        eprintln!("* config.toml is not a valid TOML file!");
        return;
    };

    let client = generator(SNS::Mastodon, String::from("https://unnerv.jp"), None, None);
    let streaming = client.local_streaming().await;
    println!("* Connected.");
    streaming
        .listen(Box::new(move |message| {
            if let Message::Update(mes) = message {
                send(&mes, config.clone());
            }
        }))
        .await;
}

fn send(mes: &Status, config: Config) {
    let parsed_content = parse_html(&mes.content);
    if !config
        .tags
        .iter()
        .any(|x| parsed_content.contains(&format!("#{x}]")))
    {
        return;
    }

    let formatted_content = parsed_content.replace(r"\#", "#").replace("  \n", "\n");
    let title = formatted_content.lines().next();
    let content = formatted_content
        .lines()
        .skip(1)
        .collect::<Vec<_>>()
        .join("\r");
    let json = if mes.media_attachments.is_empty() {
        json!({
            "username": mes.account.display_name,
            "avatar_url": mes.account.avatar,
            "embeds": [
                {
                    "title": title,
                    "description": content,
                    "url": mes.uri,
                    "timestamp": mes.created_at.to_rfc3339(),
                }
            ],
        })
    } else {
        json!({
            "username": mes.account.display_name,
            "avatar_url": mes.account.avatar,
            "embeds": [
                {
                    "title": title,
                    "description": content,
                    "url": mes.uri,
                    "timestamp": mes.created_at.to_rfc3339(),
                    "image": {
                        "url": mes.media_attachments[0].url,
                    },
                }
            ],
        })
    };
    for url in config.webhook {
        if let Err(e) = ureq::post(&url).send_json(json.clone()) {
            eprintln!("* Webhook Error: {e:?}");
        }
    }
}
